package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  
  int rows;
  int cols;
  Color[][] grid;  

  public ColorGrid(int rows, int cols) {
    grid = new Color[rows][cols];
    this.rows = rows;
    this.cols = cols;
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }
  
  @Override
  public List<CellColor> getCells() {
  List<CellColor> list = new ArrayList<>();
  for (int row = 0; row < this.rows; row++) {
    for (int col = 0; col < this.cols; col++) {
      CellPosition position = new CellPosition(row, col);
      Color color = grid[row][col];
      list.add(new CellColor(position, color));
    }
  }
  return list;
}

  @Override
  public Color get(CellPosition pos) {
    return grid[pos.row()][pos.col()];
  }

  @Override
  public void set(CellPosition pos, Color color) {
    grid[pos.row()][pos.col()] = color;
  }
}
