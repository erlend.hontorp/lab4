package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;


public class GridView extends JPanel {

  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  IColorGrid colorGrid;
  Rectangle2D outerBox;
  CellPositionToPixelConverter converter;

  public GridView(IColorGrid i) {
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = i;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    this.outerBox = calculateOuterBox();
    drawGrid(g2);
  }

  private Rectangle2D calculateOuterBox() {
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    System.out.println(getWidth());
    return new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
  }

  private void drawGrid(Graphics2D outer) {
    outer.setColor(MARGINCOLOR);
    outer.fill(outerBox);
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(outerBox, colorGrid, OUTERMARGIN);
    drawCells(outer, colorGrid, converter);
  }

  private static void drawCells(Graphics2D g2d, CellColorCollection cells, CellPositionToPixelConverter converter) {
    for (CellColor cell : cells.getCells()) {
      g2d.setColor(cell.color() != null ? cell.color() : Color.DARK_GRAY); // Hvis != null så cell.color, og else Color.DARK_GRAY
      g2d.fill(converter.getBoundsForCell(cell.cellPosition()));
    }
  }
}
